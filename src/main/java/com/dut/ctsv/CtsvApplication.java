package com.dut.ctsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CtsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(CtsvApplication.class, args);
	}

}
